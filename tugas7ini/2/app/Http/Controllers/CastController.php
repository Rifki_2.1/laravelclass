<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{

    public function index()
    {
        $data = Cast::all();
        return view('halaman.home', compact('data'));
    }


    public function create()
    {
        return view('halaman.register');
    }


    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama' => 'required',
    		'umur' => 'required',
            'bio' => 'required'

    	]);

        Cast::create([
    		'nama' => $request->nama,
    		'umur' => $request->umur,
            'bio' => $request->bio
    	]);
        return redirect('/cast');
    }


    public function show($id)
    {
        $data = Cast::find($id);
        return view('halaman.data',compact('data'));

    }


    public function edit($id)
    {
        $data = Cast::find($id);
        return view('halaman.edit',compact('data'));
    }


    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $data = Cast::find($id);
        $data->nama = $request->nama;
        $data->umur = $request->umur;
        $data->bio = $request->bio;
        $data->update();
        return redirect('/cast');
    }


    public function destroy($id)
    {
        $data = Cast::find($id);
        $data->delete();
        return redirect('/cast');
    }
}
