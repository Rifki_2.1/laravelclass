@extends('layout.master')

@section('title')
Edit Data
@endsection



@section('content')
<h2>Edit Data {{$data->id}}</h2>
        <form action="/cast/{{$data->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama Lengkap</label><br>
                <input type="text" class="form-control" value="{{$data->nama}}" id="nama" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label><br>
                <input type="number" class="form-control" id="umur" value="{{$data->umur}}" name="umur" placeholder="Msukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio :</label><br>
                <textarea name="bio" id="bio" class="form-control"rows="10"  cols="30" placeholder="Masukkan Bio">{{$data->bio}}</textarea>
                @error('bio')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
            </div>
            <input type="submit" class="btn btn-primary" value="submit">
@endsection
