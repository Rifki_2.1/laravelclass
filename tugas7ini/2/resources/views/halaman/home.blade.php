@extends('layout.master')


@section('title')
Home
@endsection

@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
@section('content')
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <a href="{{route('tambah')}}" class="btn btn-primary">+ Data Pemain</a>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Umur</th>
      <th>Bio</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($data as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>

                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-warning">Edit</a>
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>
                @endforelse
    </tbody>
    <tfoot>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Umur</th>
      <th>Bio</th>
      <th>Action</th>
    </tr>
    </tfoot>
  </table>
@endsection
