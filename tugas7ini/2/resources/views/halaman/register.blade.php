@extends('layout.master')

@section('title')
SignUp
@endsection

@section('content')
<h1>Buat Data Pemain Film Baru!</h1>
<h3>Masukkan Data</h3>
<form action="{{route('tambahAksi')}}" method="post" >
@csrf
<div class="form-group">
    <label>Nama Lengkap</label><br>
    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama">
    @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
    @enderror
</div>
<div class="form-group">
    <label>Umur</label><br>
    <input type="number" class="form-control" id="umur" name="umur" placeholder="Msukkan umur">
    @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>
<div class="form-group">
    <label>Bio :</label><br>
    <textarea name="bio" id="bio" class="form-control"rows="10" cols="30" placeholder="Masukkan Bio"></textarea>
    @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
</div>


<input type="submit" class="btn btn-primary" value="submit">

@endsection
