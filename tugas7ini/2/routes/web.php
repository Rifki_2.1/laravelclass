<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/cast',[CastController::class,'index'])->name('home');

Route::get('/cast/create',[CastController::class,'create'])->name('tambah');
Route::post('/cast',[CastController::class,'store'])->name('tambahAksi');

Route::get('/cast/{cast_id}',[CastController::class,'show'])->name('show');

Route::get('/cast/{cast_id}/edit',[CastController::class,'edit'])->name('edit');
Route::put('/cast/{cast_id}',[CastController::class,'update'])->name('editAksi');

Route::delete('/cast/{cast_id}',[CastController::class,'destroy'])->name('hapus');
